const should = require("chai").should()

const ITransform = require("@elioway/pinocchio/liar/itransform")
const Pinocchio = require("@elioway/pinocchio")

const fakeStandards = require("../utils/fake-standards")

let SEED = 123

describe("fake-standards | special fields", () => {
  it(`fakes email price value`, () => {
    let pinocchio = new Pinocchio(1)
    let lies = pinocchio.lies(
      {
        email: { ...fakeStandards.email },
        price: { ...fakeStandards.price },
        value: { ...fakeStandards.value }
      },
      SEED
    )
    lies[0].email.should.include("@")
    lies[0].price.should.be.a("number")
    lies[0].value.should.be.a("number")
  })
  it(`makes maxPrice > minPrice`, () => {
    let pinocchio = new Pinocchio(1)
    let lies = pinocchio.lies(
      {
        minPrice: { ...fakeStandards.minPrice },
        maxPrice: { ...fakeStandards.maxPrice }
      },
      SEED
    )
    lies[0].maxPrice.should.be.gt(lies[0].minPrice)
  })

  it(`makes maxValue > minValue`, () => {
    let pinocchio = new Pinocchio(1)
    let lies = pinocchio.lies(
      {
        minValue: { ...fakeStandards.minValue },
        maxValue: { ...fakeStandards.maxValue }
      },
      SEED
    )
    lies[0].maxValue.should.be.gt(lies[0].minValue)
  })
})
describe("fake-standards | long fields", () => {
  for (let longType of [
    "articleBody",
    "awards",
    "benefits",
    "commentText",
    "dependencies",
    "description",
    "educationRequirements",
    "experienceRequirements",
    "incentives",
    "incentiveCompensation",
    "jobBenefits",
    "lodgingUnitDescription",
    "qualifications",
    "responsibilities",
    "reviewBody",
    "skills",
    "text",
    "transcript"
  ]) {
    it(`fakes ${longType}`, () => {
      let pinocchio = new Pinocchio(1)
      let lies = pinocchio.lies(
        { testField: { ...fakeStandards[longType] } },
        SEED
      )
      lies.should.have.length(1)
      lies[0].testField.should.include("<h2>")
    })
  }
})
describe("fake-standards | code fields", () => {
  for (let codeType of [
    "gtin12",
    "gtin13",
    "gtin14",
    "gtin8",
    "naics",
    "postalCode",
    "flightNumber",
    "branchCode",
    "departureGate",
    "arrivalGate",
    "departureTerminal",
    "arrivalTerminal",
    "iataCode",
    "icaoCode",
    "departurePlatform",
    "arrivalPlatform",
    "busNumber",
    "issn",
    "isbn",
    "isrcCode",
    "iswcCode",
    "vehicleIdentificationNumber",
    "leiCode",
    "courseCode",
    "accessCode"
  ]) {
    it(`fakes ${codeType}`, () => {
      let pinocchio = new Pinocchio(1)
      let lies = pinocchio.lies(
        { testField: { ...fakeStandards[codeType] } },
        SEED
      )
      lies[0].testField.should.have.lengthOf(
        fakeStandards[codeType].template.length
      )
    })
  }
  it(`fakes all other types as a title (to be used a "name" for the foreign record)`, () => {
    let title = {
      class: "gossip",
      file: "ainu",
      max: 10,
      method: "paragraph",
      min: 7,
      transform: [ITransform.title]
    }
    let pinocchio = new Pinocchio(1)
    let lies = pinocchio.lies(
      { field1: title, field2: title, field3: title },
      SEED
    )
    lies[0].field1.should.eql("Isepo Ri Orowa Keray Yaku Sineani Or Mut")
    lies[0].field2.should.eql(
      "Itak Sinnam Satke Sapa Pone Kunneywaan Hosipika Hi Hureka"
    )
    lies[0].field3.should.eql("Ona Anu Akor Pompe Humas Sat Eaykap E")
  })
})
