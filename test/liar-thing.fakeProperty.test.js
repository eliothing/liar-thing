const _ = require("lodash")
const faker = require("faker")
const should = require("chai").should()

const ITransform = require("@elioway/pinocchio/liar/itransform")
const { rePurpose } = require("@elioway/pinocchio/liar/ijusthelp")
const { schemaDomainUrl } = require("@elioway/thing/utils/get-schema")

const ThingPinocchio = require("../liar-thing")
const fakeStandards = require("../utils/fake-standards")

before(done => {
  this.lieBuilder = new ThingPinocchio(
    "schemaorg/data/releases/9.0/schemaorg-all-http",
    schemaDomainUrl
  )
  done()
})

describe("liar-thing | fakeProperty | normal fields", () => {
  for (let normalType of [
    "Boolean",
    "Date",
    "DateTime",
    "Distance",
    "Duration",
    "Integer",
    "Number",
    "Quantity",
    "Time",
    "URL"
  ]) {
    it(`resolves ${normalType}`, () => {
      let expectPropertyDef = rePurpose(fakeStandards[normalType])
      // expectPropertyDef.name = "testFieldName"
      this.lieBuilder
        .fakeProperty("testFieldName", { type: normalType }, "Thing")
        .should.eql(expectPropertyDef)
    })
  }
})
describe("liar-thing | fakeProperty | long fields", () => {
  for (let longType of [
    "articleBody",
    "awards",
    "benefits",
    "commentText",
    "dependencies",
    "description",
    "educationRequirements",
    "experienceRequirements",
    "incentives",
    "incentiveCompensation",
    "jobBenefits",
    "lodgingUnitDescription",
    "qualifications",
    "responsibilities",
    "reviewBody",
    "skills",
    "text",
    "transcript"
  ]) {
    it(`resolves ${longType}`, () => {
      let expectPropertyDef = rePurpose(fakeStandards[longType])
      // expectPropertyDef.name = "testFieldName"
      this.lieBuilder
        .fakeProperty("testFieldName", { type: longType }, "Thing")
        .should.eql(expectPropertyDef)
    })
  }
})
describe("liar-thing | fakeProperty | code fields resolve", () => {
  for (let codeType of [
    "gtin12",
    "gtin13",
    "gtin14",
    "gtin8",
    "naics",
    "postalCode",
    "flightNumber",
    "branchCode",
    "departureGate",
    "arrivalGate",
    "departureTerminal",
    "arrivalTerminal",
    "iataCode",
    "icaoCode",
    "departurePlatform",
    "arrivalPlatform",
    "busNumber",
    "issn",
    "isbn",
    "isrcCode",
    "iswcCode",
    "vehicleIdentificationNumber",
    "leiCode",
    "courseCode",
    "accessCode"
  ]) {
    it(`resolves ${codeType}`, () => {
      let expectPropertyDef = rePurpose(fakeStandards[codeType])
      // expectPropertyDef.name = "testFieldName"
      this.lieBuilder
        .fakeProperty("testFieldName", { type: codeType }, "Thing")
        .should.eql(expectPropertyDef)
    })
  }
})
describe("liar-thing | fakeProperty | special fields", () => {
  for (let specialType of [
    "email",
    "minPrice",
    "maxPrice",
    "minValue",
    "maxValue",
    "price",
    "value"
  ]) {
    it(`resolves ${specialType}`, () => {
      let expectPropertyDef = rePurpose(fakeStandards[specialType])
      // expectPropertyDef.name = "testFieldName"
      this.lieBuilder
        .fakeProperty("testFieldName", { type: specialType }, "Thing")
        .should.eql(expectPropertyDef)
    })
  }
})
describe("liar-thing | fakeProperty | other fields", () => {
  it(`resolves all other types not in the standard`, () => {
    faker.seed(123)
    let propertyDef = this.lieBuilder.fakeProperty(
      "testFieldName",
      { type: "Text" },
      "Thing"
    )
    propertyDef.transform.should.be.lengthOf(2)
    propertyDef.class.should.eql("gossip")
    propertyDef.file.should.eql("ainu")
    propertyDef.max.should.eql(10)
    propertyDef.method.should.eql("paragraph")
    propertyDef.min.should.eql(7)
    // propertyDef.name.should.eql("testFieldName")
    propertyDef.transform[0]("wizard apprentice").should.eql(
      "Wizard Apprentice"
    )
    propertyDef.transform[1]("wizard apprentice1").should.eql("")
    propertyDef.transform[1]("wizard apprentice2").should.eql(
      "wizard apprentice2"
    )
    propertyDef.transform[1]("wizard apprentice3").should.eql(
      "wizard apprentice3"
    )
    propertyDef.transform[1]("wizard apprentice4").should.eql("")
    propertyDef.transform[1]("wizard apprentice5").should.eql(
      "wizard apprentice5"
    )
    propertyDef = this.lieBuilder.fakeProperty(
      "testFieldName",
      { type: "Text" },
      "Thing"
    )
    propertyDef.class.should.eql("gossip")
    propertyDef.file.should.eql("ainu")
    propertyDef.max.should.eql(10)
    propertyDef.method.should.eql("paragraph")
    propertyDef.min.should.eql(7)
    // propertyDef.name.should.eql("testFieldName")
    propertyDef = this.lieBuilder.fakeProperty(
      "testFieldName",
      { type: "Action" },
      "Thing"
    )
    propertyDef.class.should.eql("gossip")
    propertyDef.file.should.eql("ainu")
    propertyDef.max.should.eql(10)
    propertyDef.method.should.eql("paragraph")
    propertyDef.min.should.eql(7)
    // propertyDef.name.should.eql("testFieldName")
    propertyDef = this.lieBuilder.fakeProperty(
      "testFieldName",
      { type: "DumbThingDoesntExist" },
      "Thing"
    )
    propertyDef.class.should.eql("gossip")
    propertyDef.file.should.eql("ainu")
    propertyDef.max.should.eql(10)
    propertyDef.method.should.eql("paragraph")
    propertyDef.min.should.eql(7)
    // propertyDef.name.should.eql("testFieldName")
  })
})
