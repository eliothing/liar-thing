const _ = require("lodash")
const should = require("chai").should()

const ITransform = require("@elioway/pinocchio/liar/itransform")
const { schemaDomainUrl } = require("@elioway/thing/utils/get-schema")

const ThingPinocchio = require("../liar-thing")
const fakeStandards = require("../utils/fake-standards")

before(done => {
  this.lieBuilder = new ThingPinocchio(
    "schemaorg/data/releases/9.0/schemaorg-all-http",
    schemaDomainUrl
  )
  done()
})

describe("liar-thing | fakeThing", () => {
  it(`returns a valid fakeThing definition for depth 0`, () => {
    let lieDef = this.lieBuilder.fakeThing(
      {
        alternateName: { type: "Text" },
        description: { type: "Text" },
        disambiguatingDescription: { type: "Text" },
        name: { type: "Text" },
        mainEntityOfPage: { type: "URL" },
        potentialAction: { type: "Text" }
      },
      "CreativeWork"
    )
    lieDef.alternateName.class = "gossip"
    lieDef.alternateName.method = "paragraph"
    lieDef.alternateName.min = 7
    lieDef.alternateName.max = 10
    lieDef.description.should.eql({
      class: "concat",
      from: {
        heading: { class: "exact", value: "<h2>Description</h2>" },
        eliosin: {
          class: "gossip",
          method: "html",
          file: "ainu",
          html: {
            tags: ["h3", "h4"],
            max: 4,
            min: 2,
            below: {
              heavenHell: { tags: ["blockquote", "aside"], max: 5, min: 3 },
              pillar: { tags: ["p", "section"], max: 3, min: 1 },
              ul: {
                tags: ["ul", "ol"],
                max: 1,
                min: 1,
                nest: { li: { tags: ["li"], max: 6, min: 2 } }
              }
            }
          }
        }
      }
    })
    lieDef.disambiguatingDescription.class = "gossip"
    lieDef.disambiguatingDescription.method = "paragraphs"
    lieDef.disambiguatingDescription.min = 2
    lieDef.disambiguatingDescription.max = 4
    lieDef.name.class = "gossip"
    lieDef.name.method = "paragraph"
    lieDef.name.min = 7
    lieDef.name.max = 10
    lieDef.name.transform[0]("wizard's apprentice").should.eql(
      "Wizards Apprentice"
    )
    lieDef.name.transform[1]("Wizard's Apprentice").should.eql(
      "Wizard's Apprentice Creative Work"
    )
    lieDef.mainEntityOfPage.should.eql({
      class: "marak",
      mustache: "{{internet.url}}"
      // name: "mainEntityOfPage",
    })
    lieDef.potentialAction.class = "gossip"
    lieDef.potentialAction.method = "paragraph"
    lieDef.potentialAction.min = 7
    lieDef.potentialAction.max = 10
  })
  it(`returns a valid fakeThing definition for depth 1`, () => {
    let lieDef = this.lieBuilder.fakeThing(
      {
        alternateName: { type: "Text" },
        description: { type: "Text" },
        disambiguatingDescription: { type: "Text" },
        mainEntityOfPage: { type: "URL" },
        name: { type: "Text" },
        potentialAction: { type: "Action", foreign: true }
      },
      "CreativeWork"
    )
    lieDef.alternateName.class = "gossip"
    lieDef.alternateName.method = "paragraph"
    lieDef.alternateName.min = 7
    lieDef.alternateName.max = 10
    lieDef.description.should.eql({
      class: "concat",
      from: {
        heading: { class: "exact", value: "<h2>Description</h2>" },
        eliosin: {
          class: "gossip",
          method: "html",
          file: "ainu",
          html: {
            tags: ["h3", "h4"],
            max: 4,
            min: 2,
            below: {
              heavenHell: { tags: ["blockquote", "aside"], max: 5, min: 3 },
              pillar: { tags: ["p", "section"], max: 3, min: 1 },
              ul: {
                tags: ["ul", "ol"],
                max: 1,
                min: 1,
                nest: { li: { tags: ["li"], max: 6, min: 2 } }
              }
            }
          }
        }
      }
    })
    lieDef.disambiguatingDescription.class = "gossip"
    lieDef.disambiguatingDescription.method = "paragraphs"
    lieDef.disambiguatingDescription.min = 2
    lieDef.disambiguatingDescription.max = 4
    lieDef.name.class = "gossip"
    lieDef.name.method = "paragraph"
    lieDef.name.min = 7
    lieDef.name.max = 10
    lieDef.name.transform[0]("wizard's apprentice").should.eql(
      "Wizards Apprentice"
    )
    lieDef.name.transform[1]("Wizard's Apprentice").should.eql(
      "Wizard's Apprentice Creative Work"
    )
    lieDef.mainEntityOfPage.class = "gossip"
    lieDef.mainEntityOfPage.method = "paragraph"
    lieDef.mainEntityOfPage.min = 7
    lieDef.mainEntityOfPage.max = 10
    lieDef.potentialAction.class = "gossip"
    lieDef.potentialAction.method = "paragraph"
    lieDef.potentialAction.min = 7
    lieDef.potentialAction.max = 10
  })
  it(`returns a valid fakeThing with a custom defination`, () => {
    // Normally
    let lieDef = this.lieBuilder.fakeThing({ url: { type: "URL" } }, "Thing")
    lieDef.url.class = "marek"
    lieDef.url.mustache = "{{internet.url}}"
    // Using a custom definition
    lieDef = this.lieBuilder.fakeThing({ url: { type: "URL" } }, "Thing", {
      custom: "./custom/thing.js"
    })
    lieDef.url.class = "concat"
  })
})
