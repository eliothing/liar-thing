/** @usage
 */

const fs = require("fs")

const ThingBuilder = require("@elioway/thing/thing-builder")
const {
  getSchema,
  schemaDomainUrl
} = require("@elioway/thing/utils/get-schema")
let thingBuilder = new ThingBuilder(
  getSchema("9.0/schemaorg-all-http"),
  schemaDomainUrl
)

if (true) {
  for (let field of thingBuilder.FIELDS.values()) {
    let fieldTypes = [...field.types]
    field.types = new Object()
    field.models = [...field.models]
    for (let fieldType of fieldTypes) {
      if (fieldType === "Date") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.date.past().toISOString()}:CNUF`
      } else if (fieldType === "DateTime") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.date.past().toString()}:CNUF`
      } else if (fieldType === "Number") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.random.number()}:CNUF`
      } else if (fieldType === "Float") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.random.float()}:CNUF`
      } else if (fieldType === "Integer") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.random.float()}:CNUF`
      } else if (fieldType === "Text") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.lorem.words()}:CNUF`
      } else if (fieldType === "URL") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.internet.url()}:CNUF`
      } else if (fieldType === "Time") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.date.past().toLocaleTimeString()}:CNUF`
      } else if (fieldType === "Distance") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.lorem.number()}:CNUF`
      } else if (fieldType === "Quantity") {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return faker.lorem.float()}:CNUF`
      } else {
        field.types[
          fieldType
        ] = `FUNC:thingType => {return {name: faker.lorem.words(1) + ' ' + thingType + ' ${fieldType}'}}:CNUF`
      }
      // Handle enums
      let model = thingBuilder.MODELS.get(fieldType)
      if (model && model.enums.size) {
        field.types[fieldType] = `FUNC:thingType => {return _.sample(['${[
          ...model.enums
        ].join("','")}'])}:CNUF`
      }
    }
    let code =
      `const _ = require('lodash');const faker = require('faker');module.exports = ` +
      JSON.stringify(field)
    code = code.replace(/"FUNC:thingType/g, "thingType")
    code = code.replace(/}:CNUF"/g, "}")
    fs.writeFileSync(`./fields/${field.name}.js`, code)
  }
}
