<aside>
  <dl>
  <dd>Artificer of fraud; and was the first</dd>
  <dd>That practised falsehood under saintly show,</dd>
  <dd>Deep malice to conceal, couched with revenge</dd>
</dl>
</aside>

# Seeing is Believing

```
git clone https://gitlab.com/eliothing/liar-thing.git
cd liar-thing
npm i
npm run lies -- Thing --depth 1 --z 5 --custom ./custom/thing.js --seed 1234
```
