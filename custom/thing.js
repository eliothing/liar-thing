const ITransform = require("@elioway/pinocchio/liar/itransform")

/** @file Project specific definitions.
 * @tutorial Use this as a template for creating your own custom definitions.
 * @usage
 * >> let lies = this.lieBuilder.lies("Thing", {
 * >>   size: 3,
 * >>   depth: 0,
 * >>   seed: 123,
 * >>   custom: "./custom/thing.js",
 * >> })
 */
module.exports = {
  Thing: {
    url: {
      class: "concat",
      from: {
        http: { class: "exact", value: "https://" },
        www: {
          class: "exact",
          value: "www.",
          transform: [ITransform.funky.splutter(50)]
        },
        domainName: {
          class: "field",
          field: "name",
          transform: [ITransform.slugify]
        },
        randomTLD: { class: "marak", mustache: ".{{internet.domainSuffix}}" }
      }
    }
  },
  Action: {
    url: {
      class: "exact",
      value: "http://action-thing.com/"
    }
  }
}
