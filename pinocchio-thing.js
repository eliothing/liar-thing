/** @file Build fake data for Schema Things. */
"use strict"
const _ = require("lodash")

const ThingBuilder = require("@elioway/thing")
const ITransform = require("@elioway/pinocchio/liar/itransform")
const Pinocchio = require("@elioway/pinocchio")
const fs = require("fs")
const gossip = require("@elioway/pinocchio/model/gossip")
const { rePurpose } = require("@elioway/pinocchio/liar/ijusthelp")

const fakeStandards = require("./utils/fake-standards")

/**
* @usage
>>
let schemaVersion = "schemaorg/data/releases/9.0/schemaorg-all-http"
let schemaDomain = "http://schema.org/"
let thingPinocchio = new ThingPinocchio(schemaVersion, schemaDomain)
let fakeThings = thingPinocchio.lies("Thing", { depth: 0, size: 10 })
for (let fakeThing of fakeThings) {
 let mongooseThing = new Thing(fakeThing)
 fakeThing.save()
}
>>
*/
module.exports = class ThingPinocchio extends ThingBuilder {
  fakeProperty(propertyName, propertyOpts, thingAsTitle, opts) {
    let fakedProperty = new Object()
    let propertyAsTitle = _.startCase(propertyName)
    let propertyType = propertyOpts.type
    // Create a fake data handler for every property a Thing has.

    if (
      propertyOpts.enums &&
      propertyOpts.enums.length &&
      !propertyType !== "Boolean" &&
      true
    ) {
      // Enumerated types become quicklists
      fakedProperty = { class: "quick", list: [...propertyOpts.enums] }
    } else if (fakeStandards.hasOwnProperty(propertyType)) {
      // fakedProperty from predestined Type
      fakedProperty = rePurpose(fakeStandards[propertyType])
      // fakedProperty from predestined Name
    } else if (fakeStandards.hasOwnProperty(propertyName)) {
      fakedProperty = rePurpose(fakeStandards[propertyName])
    } else if (propertyName === "engage") {
      // fakedProperty from Engaged Model Type
      fakedProperty.class = "engage"
      for (let [engageTypeName, engageTypeModel] of Object.entries(
        propertyOpts
      )) {
        // Recursively call definition creation.
        fakedProperty[engageTypeName] = rePurpose(
          this.fakeThing(engageTypeModel, engageTypeName, opts)
        )
      }
    } else {
      // We don't create fakedata for any other Types (Class Types). Instead we
      // create a "name" for that Thing which we can save as that Thing and get
      // an "_id" for it.
      fakedProperty = rePurpose(gossip.ainuTitleField)
      fakedProperty.transform = [ITransform.title]
      if (["alternateName", "name"].includes(propertyName)) {
        fakedProperty.transform.push(ITransform.funky.append(thingAsTitle))
      } else {
        fakedProperty.transform.push(ITransform.funky.splutter(50))
      }
    }
    // Sprinkle the Type into the disambiguatingDescription for realism.
    if (propertyName === "disambiguatingDescription") {
      fakedProperty.transform = [
        ITransform.funky.sprinkle({
          punctuate: [thingAsTitle],
          densage: 30
        })
      ]
    }
    return fakedProperty
  }

  fakeThing(thingModel, thingModelName, opts) {
    let customFake = new Object()
    customFake[thingModelName] = new Object()
    if (!_.isEmpty(opts) && !_.isEmpty(opts.custom)) {
      customFake[thingModelName] = require(opts.custom)[thingModelName]
    }
    let fakedThing = new Object()
    let thingAsTitle = _.startCase(thingModelName)
    for (let [propertyName, propertyOpts] of Object.entries(thingModel)) {
      if (customFake[thingModelName].hasOwnProperty(propertyName)) {
        fakedThing[propertyName] = customFake[thingModelName][propertyName]
      } else {
        fakedThing[propertyName] = this.fakeProperty(
          propertyName,
          propertyOpts,
          thingAsTitle,
          opts
        )
      }
    }
    return fakedThing
  }

  lies(selectedModelName, opts) {
    let modelsMined = this.things([selectedModelName], opts)
    let lieDef = this.fakeThing(
      modelsMined[selectedModelName],
      selectedModelName,
      opts
    )
    let pinocchio = new Pinocchio(opts.size)
    return pinocchio.lies(lieDef, opts.seed)
  }
}
