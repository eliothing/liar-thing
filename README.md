![](https://elioway.gitlab.io/eliothing/liar-thing/elio-liar-Thing-logo.png)

> Make belief pretend things for development, **the elioWay**

# liar-thing ![beta](https://elioway.gitlab.io/eliosin/icon/devops/beta/favicon.ico "beta")

Generate Schema perfect random data.

- [liar-thing Documentation](https://elioway.gitlab.io/eliothing/liar-thing/)

## Installing

```
npm i @elioway/liar-thing
```

## Seeing is Believing

```
git clone https://gitlab.com/eliothing/liar-thing.git
cd liar-thing
npm i
npm run lies -- Thing --depth 1 --z 5 --custom ./custom/thing.js --seed 1234
```

- [Installing liar-thing](https://elioway.gitlab.io/eliothing/liar-thing/installing.html)

## Requirements

- [eliothing Prerequisites](https://elioway.gitlab.io/eliothing/installing.html)

## Nutshell

- [eliothing Quickstart](https://elioway.gitlab.io/eliothing/quickstart.html)
- [liar-thing Quickstart](https://elioway.gitlab.io/eliothing/liar-thing/quickstart.html)

# Credits

- [liar-thing Credits](https://elioway.gitlab.io/eliothing/liar-thing/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/eliothing/liar-thing/apple-touch-icon.png)
