"use strict"
const _ = require("lodash")
const faker = require("faker")

const { rePurpose } = require("@elioway/pinocchio/liar/ijusthelp")
// const business = require("@elioway/pinocchio/model/business")
const common = require("@elioway/pinocchio/model/common")
const date = require("@elioway/pinocchio/model/date")
const float = require("@elioway/pinocchio/model/float")
const gossip = require("@elioway/pinocchio/model/gossip")
const int = require("@elioway/pinocchio/model/int")
// const location = require("@elioway/pinocchio/model/location")
// const marketing = require("@elioway/pinocchio/model/marketing")
// const personal = require("@elioway/pinocchio/model/personal")
const primitive = require("@elioway/pinocchio/model/primitive")
// const raw = require("@elioway/pinocchio/model/raw")
const time = require("@elioway/pinocchio/model/time")

let LONG_TEXT_FIELDS = [
  "articleBody",
  "awards",
  "benefits",
  "commentText",
  "dependencies",
  "description",
  "educationRequirements",
  "experienceRequirements",
  "incentives",
  "incentiveCompensation",
  "jobBenefits",
  "lodgingUnitDescription",
  "qualifications",
  "responsibilities",
  "reviewBody",
  "skills",
  "text",
  "transcript"
].reduce((obj, fieldName) => {
  obj[fieldName] = {
    class: "concat",
    from: {
      heading: {
        class: "exact",
        value: `<h2>${_.startCase(fieldName)}</h2>`
      },
      eliosin: gossip.eliosin
    }
  }
  return obj
}, {})

let CODE_TEXT_FIELDS = Object.entries({
  gtin12: 12,
  gtin13: 13,
  gtin14: 14,
  gtin8: 8,
  naics: 6,
  postalCode: 30,
  flightNumber: 10,
  branchCode: 10,
  departureGate: 10,
  arrivalGate: 10,
  departureTerminal: 10,
  arrivalTerminal: 10,
  iataCode: 3,
  icaoCode: 4,
  departurePlatform: 10,
  arrivalPlatform: 10,
  busNumber: 10,
  issn: 8,
  isbn: 13,
  isrcCode: 50,
  iswcCode: 50,
  vehicleIdentificationNumber: 17,
  leiCode: 50,
  courseCode: 50,
  accessCode: 50,
  identifier: 0
}).reduce((obj, [fieldName, codeLen]) => {
  obj[fieldName] = {
    class: "primitive",
    method: "uuid",
    template: "h".repeat(codeLen)
  }
  return obj
}, {})

let SPECIAL_FIELDS = {
  email: common.emailField,
  minPrice: float.gadgetPriceField,
  maxPrice: {
    class: "field",
    field: "minPrice",
    transform: [val => val + faker.random.float({ min: 10, max: 1000 })]
  },
  minValue: int.of1000Field,
  maxValue: {
    class: "field",
    field: "minValue",
    transform: [val => val + faker.random.number({ min: 10, max: 1000 })]
  },
  price: float.gadgetPriceField,
  value: int.of1000Field,
  disambiguatingDescription: rePurpose(gossip.ainuTitleField, {
    method: "paragraphs",
    min: 2,
    max: 3
  })
}

module.exports = {
  ...LONG_TEXT_FIELDS,
  ...CODE_TEXT_FIELDS,
  ...SPECIAL_FIELDS,
  Boolean: primitive.boolField,
  Date: primitive.dateField,
  DateTime: primitive.dateField,
  Distance: float.gadgetPriceField,
  Duration: float.groceryPriceField,
  Integer: int.of1000Field,
  Number: int.of10000Field,
  Quantity: int.of100Field,
  Time: time.withinMinutesField,
  URL: common.uRLField
}
