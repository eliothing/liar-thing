#!/usr/bin/env node
const commander = require("commander")
const faker = require("faker")
const fs = require("fs")

function myParseInt(value, dummyPrevious) {
  // parseInt takes a string and an optional radix
  return parseInt(value)
}

commander
  .version("1.0.0", "-v, --version")
  .usage("[OPTIONS]...")
  .option(
    "-z, --size <integer>",
    "The number of records you want.",
    myParseInt,
    1
  )
  .option(
    "-d, --depth <integer>",
    "How deep to mine for related Types",
    myParseInt,
    0
  )
  .option(
    "-s, --seed <integer>",
    "Use the same Seed for the Things which will always return the same random things! Really!",
    myParseInt,
    0
  )
  .option(
    "-c, --custom <string>",
    "The path to a file with project specific definitions for selected Thing types and fields."
  )
  .parse(process.argv)

const ThingPinocchio = require("../liar-thing")
const { schemaDomainUrl } = require("@elioway/thing/utils/get-schema")

if (!commander.args) {
  throw new Error(
    "What kind of fake Things do you want? Add one or more Thing Type to your call, e.g. `npm run lies -- Thing --depth 1 --size 10`"
  )
}

let thingType = commander.args[0]
let opts = commander.opts()

let lieBuilder = new ThingPinocchio(
  "schemaorg/data/releases/9.0/schemaorg-all-http",
  schemaDomainUrl
)

let lies = lieBuilder.lies(thingType, opts)
fs.writeFileSync(
  `./pretty/ugly/${commander.args.join("-")}-${opts.depth}.json`,
  JSON.stringify(lies)
)
